from django.forms import ModelForm
from todos.models import TodoList, TodoItem
from django import forms


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemCreateForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
