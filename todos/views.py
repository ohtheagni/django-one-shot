from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemCreateForm, TodoItemForm
from django.views.generic import UpdateView, CreateView
from django.urls import reverse


# Create your views here.
def show_list(request):
    todolist = TodoList.objects.all()
    context = {"todolist": todolist}
    return render(request, "todos/list.html", context)


def list_detail(request, id):
    todolist = TodoList.objects.get(id=id)
    tasks = TodoItem.objects.filter(list=todolist)
    context = {
        "todolist": todolist,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
        context = {
            "form": form,
            "todolist": todolist,
        }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html", {"todolist": todolist})


def todo_item_create(request):
    form = TodoItemCreateForm()
    if request.method == "POST":
        form = TodoItemCreateForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    return render(request, "todos/createitem.html", {"form": form})


class TodoItemUpdate(UpdateView):
    model = TodoItem
    form_class = TodoItemForm
    template_name = "todos/todo_item_form.html"

    def form_valid(self, form):
        form.save()
        return redirect(
            reverse("todo_list_detail", kwargs={"id": form.instance.list.id})
        )
